var members = [
  {
    "image": "images/Melissa_Huang",
    "name": "Melissa Huang"
  },
  {
    "image": "images/Kenneth_Jeffris",
    "name": "Kenneth Jeffris"
  },
  {
    "image": "images/Brandon_Leung",
    "name": "Brandon Leung"
  },
  {
    "image": "images/Albert_Hu",
    "name": "Albert Hu"
  },
  {
    "image": "images/Steven_Yang",
    "name": "Steven Yang"
  },
  {
    "image": "images/Rajeev_Udumula",
    "name": "Rajeev Udumula"
  },
  {
    "image": "images/Gabriel_Kaufman",
    "name": "Gabriel Kaufman"
  },
  {
    "image": "images/Jim_Hsiao",
    "name": "Jim Hsiao"
  },
  {
    "image": "images/Cyrus-Jan_Batino",
    "name": "Cyrus-Jan Batino"
  },
  {
    "image": "images/Saeam_Kwon",
    "name": "Saeam Kwon"
  },
  {
    "image": "images/Samantha_Cheng",
    "name": "Samantha Cheng"
  },
  {
    "image": "images/Amy_Chiang",
    "name": "Amy Chiang"
  },
  {
    "image": "images/Elton_Lou",
    "name": "Elton Lou"
  },
  {
    "image": "images/Justin_Jeong",
    "name": "Justin Jeong"
  },
  {
    "image": "images/Brian_Tseng",
    "name": "Brian Tseng"
  },
  {
    "image": "images/Andrew_Veenstra",
    "name": "Andrew Veenstra"
  },
  {
    "image": "images/Nathan_Soufer",
    "name": "Nathan Soufer"
  },
  {
    "image": "images/Daniel_Au",
    "name": "Daniel Au"
  },
  {
    "image": "images/Jonathan_Chu",
    "name": "Jonathan Chu"
  },
  {
    "image": "images/Jae_Kyung_Chong",
    "name": "Jae Kyung Chong"
  },
  {
    "image": "images/Michael_Do",
    "name": "Michael Do"
  },
  {
    "image": "images/Jeff_Gong",
    "name": "Jeff Gong"
  },
  {
    "image": "images/Kai_Li",
    "name": "Kai Li"
  },
  {
    "image": "images/Trish_Yeh",
    "name": "Trish Yeh"
  },
  {
    "image": "images/Scott_Wun",
    "name": "Scott Wun"
  },
  {
    "image": "images/Nik_Bisht",
    "name": "Nik Bisht"
  },
  {
    "image": "images/Jisoo_Han",
    "name": "Jisoo Han"
  },
  {
    "image": "images/Amanda_Xia",
    "name": "Amanda Xia"
  },
  {
    "image": "images/Emilee_Chen",
    "name": "Emilee Chen"
  },
  {
    "image": "images/Xavier_Redondo",
    "name": "Xavier Redondo"
  },
  {
    "image": "images/Elise_Christine_Lim",
    "name": "Elise Christine Lim"
  },
  {
    "image": "images/Sumeet_Jain",
    "name": "Sumeet Jain"
  },
  {
    "image": "images/Alex_Francis",
    "name": "Alex Francis"
  },
  {
    "image": "images/Grace_Alexander",
    "name": "Grace Alexander"
  },
  {
    "image": "images/Jonathan_Feng",
    "name": "Jonathan Feng"
  },
  {
    "image": "images/Betty_Hu",
    "name": "Betty Hu"
  },
  {
    "image": "images/Angela_So",
    "name": "Angela So"
  },
  {
    "image": "images/Jee_Woong_Steve_Choi",
    "name": "Jee Woong Steve Choi"
  },
  {
    "image": "images/Nancy_Chang",
    "name": "Nancy Chang"
  },
  {
    "image": "images/Leo_Ko",
    "name": "Leo Ko"
  },
  {
    "image": "images/Silvia_Zannetti",
    "name": "Silvia Zannetti"
  },
  {
    "image": "images/Satoko_Ayabe",
    "name": "Satoko Ayabe"
  },
  {
    "image": "images/Jessica_Murphy",
    "name": "Jessica Murphy"
  },
  {
    "image": "images/Koji_Habu",
    "name": "Koji Habu"
  },
  {
    "image": "images/Joana_Cabrera",
    "name": "Joana Cabrera"
  },
  {
    "image": "images/James_Ho",
    "name": "James Ho"
  },
  {
    "image": "images/Julia_Li",
    "name": "Julia Li"
  },
  {
    "image": "images/Jesse_Luo",
    "name": "Jesse Luo"
  },
  {
    "image": "images/Katie_Chen",
    "name": "Katie Chen"
  },
  {
    "image": "images/Hanna_Lee",
    "name": "Hanna Lee"
  },
  {
    "image": "images/Emilie_Gao",
    "name": "Emilie Gao"
  },
  {
    "image": "images/Kevin_Lu",
    "name": "Kevin Lu"
  },
  {
    "image": "images/Calvin_Yan",
    "name": "Calvin Yan"
  },
  {
    "image": "images/Michael_Im",
    "name": "Michael Im"
  },
  {
    "image": "images/Nicole_Rahal",
    "name": "Nicole Rahal"
  },
  {
    "image": "images/Andrew_Cho",
    "name": "Andrew Cho"
  },
  {
    "image": "images/Evan_Lee",
    "name": "Evan Lee"
  },
  {
    "image": "images/Meg_Holtzinger",
    "name": "Meg Holtzinger"
  },
  {
    "major": "Mechanical Engineering",
    "name": "Jason Cheung",
    "keepsake": "Multitool",
    "hometown": "Castro Valley, CA",
    "image": "images/Jason_Cheung",
    "address": "2540 Regent St",
    "year": "2nd",
    "pledge": "TRUE"
  },
  {
    "major": "Industrial Engineering Operations Research",
    "name": "Aimee (Mimi) Largier",
    "keepsake": "silver ring ",
    "hometown": "Sebastopol, CA",
    "image": "images/Aimee_Largier",
    "address": "2601 Warring St. (Buidling 4, Room 210)",
    "year": "1st",
    "pledge": "TRUE"
  },
  {
    "major": "Computer Science (Intended)",
    "name": "Florence Lau",
    "keepsake": "Medicine from Hong Kong",
    "hometown": "Cerritos, CA",
    "image": "images/Florence_Lau",
    "address": "2650 Haste Street (Griffiths, Room 307)",
    "year": "1st",
    "pledge": "TRUE"
  },
  {
    "major": "Electrical Engineering and Computer Science",
    "name": "Andrew Liu",
    "keepsake": "Dad's watch",
    "hometown": "Houston, TX",
    "image": "images/YuXuan_(Andrew)_Liu",
    "address": "2423 Blake St",
    "year": "2nd",
    "pledge": "TRUE"
  },
  {
    "major": "Bioengineering",
    "name": "Samson Mataraso",
    "keepsake": "Math socks",
    "hometown": "Walnut Creek, CA",
    "image": "images/Samson_Mataraso",
    "address": "2650 Haste Street, Ehrman 207 (Haste and College)",
    "year": "1st",
    "pledge": "TRUE"
  },
  {
    "major": "Computer Science/Statistics/Mathematics",
    "name": "Vignesh Muruganantham",
    "keepsake": "Watch",
    "hometown": "Fremont, CA",
    "image": "images/Vignesh_Muruganantham",
    "address": "2202 Blake Street (Blake, Fulton)",
    "year": "2nd",
    "pledge": "TRUE"
  },
  {
    "major": "Computer Science / Business Administration (Intended)",
    "name": "Albert Phone",
    "keepsake": "Pikachu Doll",
    "hometown": "Diamond Bar, CA",
    "image": "images/Albert_Phone",
    "address": "2535 College Ave. (Parker, College",
    "year": "2nd",
    "pledge": "TRUE"
  },
  {
    "major": "Mechanical Engineering",
    "name": "Rachelle Rosano",
    "keepsake": "Spidey keychain",
    "hometown": "Imperial Beach, CA",
    "image": "images/Rachelle_Rosano",
    "address": "2414 Parker Street (Parker and Telegraph)",
    "year": "2nd",
    "pledge": "TRUE"
  },
  {
    "major": "Intended Physics and Computer Science",
    "name": "Phoebe So",
    "keepsake": "Totoro stuffed animal",
    "hometown": "Palo Alto, CA",
    "image": "images/Phoebe_So",
    "address": "2437 Piedmont Ave, Apt 206 (Piedmont and Dwight)",
    "year": "2nd",
    "pledge": "TRUE"
  },
  {
    "major": "Mechanical Engineering",
    "name": "Dessislava (Dessie) Stamatova",
    "keepsake": "Chinese fan",
    "hometown": "Chicago, IL",
    "image": "images/Dessie_Stamatova",
    "address": "2650 Haste Street (CU #611)",
    "year": "1st",
    "pledge": "TRUE"
  },
  {
    "major": "Computer Science",
    "name": "Warren Tian",
    "keepsake": "Penguin the Panda",
    "hometown": "Fremont, CA",
    "image": "images/Warren_Tian",
    "address": "2535 College Ave. (Parker, College)",
    "year": "2nd",
    "pledge": "TRUE"
  },
  {
    "major": "Music / Intended Computer Science",
    "name": "Annie Tsai",
    "keepsake": "Buddhist emblem",
    "hometown": "Victorville, CA",
    "image": "images/Annie_Tsai",
    "address": "2515 Benvenue Ave #208 (Dwight and Benvenue)",
    "year": "2nd",
    "pledge": "TRUE"
  },
  {
    "major": "Mechanical Engineering",
    "name": "Richard Vuu",
    "keepsake": "Nerf Darts",
    "hometown": "Seattle, WA",
    "image": "images/Richard_Vuu",
    "address": "2312 Warring Street",
    "year": "2nd",
    "pledge": "TRUE"
  },
  {
    "major": "Intended Computer Science",
    "name": "Mike Wang",
    "keepsake": "Weener Kleener Soap",
    "hometown": "San Ramon, CA",
    "image": "images/Mike_Wang",
    "address": "2650 DURANT AVE (PUTNAM ROOM 208)",
    "year": "1st",
    "pledge": "TRUE"
  },
  {
    "major": "Intended Computer Science",
    "name": "Sally Yen",
    "keepsake": "100 Year of Solitude",
    "hometown": "San Diego, CA",
    "image": "images/Sally_Yen",
    "address": "2400 Durant Ave (Spens-Black #301)",
    "year": "1st",
    "pledge": "TRUE"
  }
]


function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex ;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

function get_ten () {
  return shuffle_members().slice(0,10);
}

function shuffle_members() {
  return shuffle($.extend(true, [], members));
}

function get_pledges () {
  return shuffle($.extend(true, [], members)).filter(function (person) { 
    return 'pledge' in person && person.pledge
  });
}


var app = angular.module('flashcards', []);

app.controller('cardController', function($scope) {
  function reset(e) {
    shuffled_members = shuffle_members();
    $scope.totalNum = members.length;
    $scope.currentCorrect = 0;
    $scope.currentMember = shuffled_members[0];
    $scope.display = false;
    $scope.pledge = false;
    wrong_members = [];
    $scope.currentWrong = wrong_members.length;
    if (e) {
      e.target.blur();
    }
  }
  
  reset();

  $scope.doTen = function (e) {
    reset();
    shuffled_members = get_ten();
    $scope.totalNum = shuffled_members.length;
    $scope.currentMember = shuffled_members[0];
    e.target.blur();
  }

  $scope.reset = reset;

  $scope.getPledges = function (e) {
    reset();
    $scope.pledge = true;
    shuffled_members = get_pledges();
    $scope.totalNum = shuffled_members.length;
    $scope.currentMember = shuffled_members[0];
    e.target.blur();
  }


  $scope.changeCard = function(keyEvent) {
    var code = keyEvent.which || keyEvent.keyCode,
        BAD = 32, // space
        nextCodes = [
          BAD,
          39,
          37
        ];

    if (nextCodes.indexOf(code) != -1) {
      if ($scope.display) {
        var last = shuffled_members.shift();
        if (code == BAD) {
          wrong_members.push(last);
          $scope.currentWrong = wrong_members.length;
        } else {
          $scope.currentCorrect++;
        }
        if (!shuffled_members.length) {
          if (wrong_members.length) {
            shuffled_members = wrong_members;
            wrong_members = [];
          } else {
            shuffled_members = [{'name': 'All done', 'image': null}];
          }
        }
        $scope.currentMember = shuffled_members[0];
        $scope.display = false;
      } else {
        $scope.display = true;
      }
    }
  }
});
