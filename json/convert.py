import csv, json

with open('info.csv', 'rb') as csvfile:
    data = [{
        "name" : row["Name"],
        "hometown" : row["Hometown (City, State)"],
        "keepsake" : row["Sentimental Item"],
        "year" : row["Year"],
        "address" : row["Address/Cross Streets"],
        "major" : row["Major"],
        "pledge" : "True",
        "image" : "images/"+ row["Name"].replace(' ', '_')
    } for row in csv.DictReader(csvfile)]
    with open('pledges.json', 'w') as out_file:
        json.dump(data, out_file)
