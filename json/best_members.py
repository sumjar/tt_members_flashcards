import json
from StringIO import StringIO

with open('pretty_members.json', 'r') as f_:
    members_dict = json.load(f_)
    better_dict = [{'name': name, 'image': 'images/%s' % name.replace(' ','_') }
                    for name, _ in members_dict.items()]
    with open('best_members.json', 'w') as out:
        out.write(json.dumps(better_dict, sort_keys=True))
