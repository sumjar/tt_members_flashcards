import json
import requests
import shutil

with open('pretty_members.json', 'r') as f_:
    members_dict = json.load(f_)
    for name, url in members_dict.items():
        resp = requests.get(url, stream=True)
        if resp.status_code == 200:
            path = 'images/' + name.replace(' ', '_')
            print "Downloading %s" % path
            with open(path, 'wb') as f:
                resp.raw.decode_content = True
                shutil.copyfileobj(resp.raw, f)
